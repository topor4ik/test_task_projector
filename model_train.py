import pandas as pd
import numpy as np
from tqdm import tqdm
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from lightgbm import LGBMRegressor, early_stopping
from transformers import BertModel, BertTokenizerFast
import joblib

# read data
train = pd.read_csv('data/train.csv')
test = pd.read_csv('data/test.csv')
sample = pd.read_csv('data/sample_submission.csv')

# encode bert model from tokenizer
def bert_encode(texts, tokenizer, model):
    vectors = []
    
    for text in tqdm(texts):
        encoded_input = tokenizer(text, return_tensors='pt')
        output = model(**encoded_input)
        
        vectors.append(output[1].detach().numpy()[0])
    
    return np.array(vectors)

tokenizer = BertTokenizerFast.from_pretrained("bert-large-uncased")
model = BertModel.from_pretrained("bert-base-uncased")

# transform text into vectors
print('Text vectorization...')
X = bert_encode(train['excerpt'].values, tokenizer, model)
X_test = bert_encode(test['excerpt'].values, tokenizer, model)
y = train['target'].values

print(X.shape, X_test.shape, y.shape)

# split train val
X_train, X_val, y_train, y_val = train_test_split(
    X, y, test_size=0.2, random_state=42)
print(X_train.shape, X_val.shape, y_train.shape, y_val.shape)

# build model
print('Model training...')
params = {
    'boosting_type': 'gbdt',
      'random_state': 21
}

gbm = LGBMRegressor(**params).fit(X_train, y_train, 
                                   verbose=50,
                                   eval_set=[(X_train, y_train), (X_val, y_val)],
                                   callbacks=[early_stopping(20)],
                                   eval_metric=['rmse'])

# check metrics
print('Train RMSE:      ', mean_squared_error(y_train, gbm.predict(X_train)))
print('Validation RMSE: ', mean_squared_error(y_val, gbm.predict(X_val)))

# save lgb model
model_file_name = "DEMO-local-lgb-model"
joblib.dump(gbm, 'models/'+model_file_name) 
print('The end!')

