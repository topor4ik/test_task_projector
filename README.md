# Project for test task (Machine Learning in Production course)



## Getting started

To reproduce project results create virtualenv with packages from requirements.txt:
```
1. conda create --name prjctr_env
2. conda activate prjctr_env
3. pip install -r requirements.txt
```
Load data for project into "data" directory from https://www.kaggle.com/competitions/commonlitreadabilityprize/data

## Stage 1 (create model)
To create ML model run python script 

```
python model_train.py
```
ML model metrics:
```
Train RMSE:       0.024
Validation RMSE:  0.462
```
Trained model saved in "models" directory (file: DEMO-local-lgb-model).

## Stage 2 (write an API)
To start API run:
```
python server.py
```
API is running under your localhost: localhost:5000/api

You can test API and send POST on localhost:5000/api with Postman in format:
```
{"text": "Put your here!"}
```

![My Image](images/screen.jpeg)

## Stage 3 (Deploy API )
TBD
