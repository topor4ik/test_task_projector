# Import libraries
import numpy as np
from flask import Flask, request, jsonify
import joblib
from transformers import BertModel, BertTokenizerFast
import lightgbm

app = Flask(__name__)

# encode bert model from tokenizer
def bert_encode_inference(text, tokenizer, model):
    encoded_input = tokenizer(text, return_tensors='pt')
    output = model(**encoded_input)
    return output[1].detach().numpy()[0]

# load text processors
tokenizer = BertTokenizerFast.from_pretrained("bert-large-uncased")
model = BertModel.from_pretrained("bert-base-uncased")

# Load the model
model_file_name = "DEMO-local-lgb-model"
gbm = joblib.load('models/'+model_file_name)

@app.route('/api',methods=['POST'])
def predict():
    # Get the data from the POST request.
    data = request.get_json(force=True)
    # vectorize text
    X = bert_encode_inference(data['text'], tokenizer, model).reshape(1, -1)
    # Make prediction using model loaded from disk
    prediction = gbm.predict(X)
    return jsonify(prediction.tolist())

if __name__ == '__main__':
    app.run(port=5000, debug=True)